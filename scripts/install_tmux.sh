#!/bin/bash

# Update submodules (if any)
git submodule update --init --recursive

# Tmux config file
rm -rf $HOME/.tmux.conf
ln -s `pwd`/tmux.conf $HOME/.tmux.conf
